using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using Sandbox.Game;
using Sandbox.ModAPI;
using VRage;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.ModAPI;
using VRage.Utils;
using VRageMath;
using static Scripts.Structure;
namespace Scripts
{
    [MySessionComponentDescriptor(MyUpdateOrder.NoUpdate, int.MaxValue)]
    public class Session : MySessionComponentBase
    {
        public override void LoadData()
        {
            Log.Init($"{ModContext.ModName}Init.log");
            MyAPIGateway.Utilities.RegisterMessageHandler(7772, Handler);
            Init();
            SendModMessage(true);
        }

        protected override void UnloadData()
        {
            Log.Close();
            MyAPIGateway.Utilities.UnregisterMessageHandler(7772, Handler);
            Array.Clear(Storage, 0, Storage.Length);
            Storage = null;
        }

        void Handler(object o)
        {
            if (o == null) SendModMessage(false);
        }

        void SendModMessage(bool sending)
        {
            Log.CleanLine(sending ? $"Sending request to core " : $"Receiving request from core");
            MyAPIGateway.Utilities.SendModMessage(7771, Storage);
        }

        internal byte[] Storage;

        internal void Init()
        {
            ContainerDefinition baseDefs;
            Parts.GetBaseDefinitions(out baseDefs);
            Parts.SetModPath(baseDefs, ModContext.ModPath);
            SetValuesFromFile(baseDefs);
            Storage = MyAPIGateway.Utilities.SerializeToBinary(baseDefs);

            //PrintWepAmmoLink(baseDefs);
            //ExportAmmo(baseDefs);
            //ExportWeapons(baseDefs);

            Log.CleanLine($"fgs: Handing over control to Core and going to sleep");
        }


        private void SetValuesFromFile(ContainerDefinition baseDefs)
        {
            try
            {
                foreach (var mod in MyAPIGateway.Session.Mods)
                {
                    if (MyAPIGateway.Utilities.FileExistsInModLocation("Data/WC_CustomConfig.xml", mod))
                    {
                        using (var reader = MyAPIGateway.Utilities.ReadFileInModLocation("Data/WC_CustomConfig.xml", mod))
                        {
                            Modifiers modifers = MyAPIGateway.Utilities.SerializeFromXML<Modifiers>(reader.ReadToEnd());


                            foreach (var ammo in modifers.Ammos)
                            {
                                FLog($"AMMO1: {ammo.AmmoName}");
                            }

                            HashSet<string> names = new HashSet<string>();
                            foreach (var ammo in baseDefs.WeaponDefs)
                            {
                                foreach (var ammoDef in ammo.Ammos)
                                {
                                    names.Add(ammoDef.AmmoRound);
                                }

                            }

                            foreach (var name in names)
                            {
                                FLog($"AMMO2: {name}");
                            }



                            foreach (var wp in modifers.Weapons)
                            {
                                FLog($"WP1: {wp.PartName}");
                            }
                            foreach (var wp in baseDefs.WeaponDefs)
                            {
                                FLog($"WP2: {wp.HardPoint.PartName}");
                            }

                            foreach (var ammodef in modifers.Ammos)
                            {
                                SetAmmoDefMod(ammodef, baseDefs);
                            }

                            foreach (var weaponDef in modifers.Weapons)
                            {
                                SetWeaponDefMod(weaponDef, baseDefs);
                            }

                            SLog("Applied modifiers successfully");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SLog("fgs: SetValuesFromFile error: " + ex.ToString());
            }
        }

        private void SLog(string txt)
        {

            MyLog.Default.WriteLine("SLOG:" + txt);
        }

        private void FLog(string txt)
        {
            MyLog.Default.WriteLine("FLOG:" + txt);
        }

        private void FError(string txt)
        {
            MyLog.Default.WriteLine("FERROR:" + txt);
        }


        private void ApplyAmmo(Modifiers.AmmoMod ammodef, WeaponDefinition.AmmoDef targetAmmo)
        {
            try
            {
                switch (ammodef.Variable)
                {
                    case "EnergyCost":
                        targetAmmo.EnergyCost = float.Parse(ammodef.Value);
                        break;
                    case "Trajectory/MaxTrajectory":
                        targetAmmo.Trajectory.MaxTrajectory = float.Parse(ammodef.Value);
                        break;
                    case "Trajectory/DesiredSpeed":
                        targetAmmo.Trajectory.DesiredSpeed = float.Parse(ammodef.Value);
                        break;
                    case "BaseDamage":
                        targetAmmo.BaseDamage = float.Parse(ammodef.Value);
                        break;
                    case "DamageScales/DamageType/Base":
                        targetAmmo.DamageScales.DamageType.Base = (WeaponDefinition.AmmoDef.DamageScaleDef.DamageTypes.Damage)Enum.Parse(typeof(WeaponDefinition.AmmoDef.DamageScaleDef.DamageTypes.Damage), ammodef.Value, true);
                        break;
                    case "DamageScales/DamageType/AreaEffect":
                        targetAmmo.DamageScales.DamageType.AreaEffect = (WeaponDefinition.AmmoDef.DamageScaleDef.DamageTypes.Damage)Enum.Parse(typeof(WeaponDefinition.AmmoDef.DamageScaleDef.DamageTypes.Damage), ammodef.Value, true);
                        break;
                    case "DamageScales/DamageType/Detonation":
                        targetAmmo.DamageScales.DamageType.Detonation = (WeaponDefinition.AmmoDef.DamageScaleDef.DamageTypes.Damage)Enum.Parse(typeof(WeaponDefinition.AmmoDef.DamageScaleDef.DamageTypes.Damage), ammodef.Value, true);
                        break;
                    case "Fragment/Fragments":
                        targetAmmo.Fragment.Fragments = int.Parse(ammodef.Value);
                        break;
                    case "Fragment/AmmoRound":
                        targetAmmo.Fragment.AmmoRound = ammodef.Value;
                        break;
                    case "Fragment/Degrees":
                        targetAmmo.Fragment.Degrees = int.Parse(ammodef.Value);
                        break;
                    case "Fragment/Reverse":
                        targetAmmo.Fragment.Reverse = bool.Parse(ammodef.Value);
                        break;
                    case "EnergyMagazineSize":
                        targetAmmo.EnergyMagazineSize = int.Parse(ammodef.Value);
                        break;
                    case "AreaOfDamage/ByBlockHit/Enable":
                        targetAmmo.AreaOfDamage.ByBlockHit.Enable = bool.Parse(ammodef.Value);
                        break;
                    case "AreaOfDamage/ByBlockHit/Radius":
                        targetAmmo.AreaOfDamage.ByBlockHit.Radius = double.Parse(ammodef.Value);
                        break;
                    case "AreaOfDamage/ByBlockHit/Damage":
                        targetAmmo.AreaOfDamage.ByBlockHit.Damage = float.Parse(ammodef.Value);
                        break;
                    case "AreaOfDamage/ByBlockHit/Depth":
                        targetAmmo.AreaOfDamage.ByBlockHit.Depth = float.Parse(ammodef.Value);
                        break;
                    case "AreaOfDamage/EndOfLife/Enable":
                        targetAmmo.AreaOfDamage.EndOfLife.Enable = bool.Parse(ammodef.Value);
                        break;
                    case "AreaOfDamage/EndOfLife/Radius":
                        targetAmmo.AreaOfDamage.EndOfLife.Radius = double.Parse(ammodef.Value);
                        break;
                    case "AreaOfDamage/EndOfLife/Damage":
                        targetAmmo.AreaOfDamage.EndOfLife.Damage = float.Parse(ammodef.Value);
                        break;
                    case "AreaOfDamage/EndOfLife/Depth":
                        targetAmmo.AreaOfDamage.EndOfLife.Depth = float.Parse(ammodef.Value);
                        break;
                    case "AreaOfDamage/EndOfLife/Falloff":
                        targetAmmo.AreaOfDamage.EndOfLife.Falloff = (WeaponDefinition.AmmoDef.AreaOfDamageDef.Falloff)Enum.Parse(typeof(WeaponDefinition.AmmoDef.AreaOfDamageDef.Falloff), ammodef.Value);
                        break;
                    case "AreaOfDamage/EndOfLife/ParticleScale":
                        targetAmmo.AreaOfDamage.EndOfLife.ParticleScale = float.Parse(ammodef.Value);
                        break;
                    case "Mass":
                        targetAmmo.Mass = float.Parse(ammodef.Value);
                        break;
                    case "DamageScales/Grids/Large":
                        targetAmmo.DamageScales.Grids.Large = float.Parse(ammodef.Value);
                        break;
                    case "DamageScales/Grids/Small":
                        targetAmmo.DamageScales.Grids.Small = float.Parse(ammodef.Value);
                        break;
                    case "DamageScales/Armor/Armor":
                        targetAmmo.DamageScales.Armor.Armor = float.Parse(ammodef.Value);
                        break;
                    case "DamageScales/Armor/Heavy":
                        targetAmmo.DamageScales.Armor.Heavy = float.Parse(ammodef.Value);
                        break;
                    case "DamageScales/Armor/Light":
                        targetAmmo.DamageScales.Armor.Light = float.Parse(ammodef.Value);
                        break;
                    case "DamageScales/Armor/NonArmor":
                        targetAmmo.DamageScales.Armor.NonArmor = float.Parse(ammodef.Value);
                        break;
                    case "DamageScales/FallOff/Distance":
                        targetAmmo.DamageScales.FallOff.Distance = float.Parse(ammodef.Value);
                        break;
                    case "DamageScales/FallOff/MinMultipler":
                        targetAmmo.DamageScales.FallOff.MinMultipler = float.Parse(ammodef.Value);
                        break;
                    case "DamageScales/HealthHitModifier":
                        targetAmmo.DamageScales.HealthHitModifier = double.Parse(ammodef.Value);
                        break;
                    case "Trajectory/Smarts/Inaccuracy":
                        targetAmmo.Trajectory.Smarts.Inaccuracy = double.Parse(ammodef.Value);
                        break;
                    case "Trajectory/GravityMultiplier":
                        targetAmmo.Trajectory.GravityMultiplier = float.Parse(ammodef.Value);
                        break;
                    default:
                        FError($"ERROR default! [{ammodef.Variable}] Свойство не добавлено в импортер.");
                        break;
                }
            }
            catch (Exception ex)
            {
                SLog($"fgs: weapondef.PartName [{ammodef.Variable}] throw ex: {ex.Message}  u use float when expected int? ");
            }

        }

        private void ApplyWeapon(Modifiers.WeaponMod weapondef, WeaponDefinition target)
        {
            try
            {
                switch (weapondef.Variable)
                {
                    case "Targeting/MaxTargetDistance":
                        target.Targeting.MaxTargetDistance = float.Parse(weapondef.Value);
                        break;
                    case "Targeting/MinTargetDistance":
                        target.Targeting.MinTargetDistance = float.Parse(weapondef.Value);
                        break;
                    case "Targeting/IgnoreDumbProjectiles":
                        target.Targeting.IgnoreDumbProjectiles = bool.Parse(weapondef.Value);
                        break;
                    case "HardPoint/Loading/ReloadTime":
                        target.HardPoint.Loading.ReloadTime = int.Parse(weapondef.Value);
                        break;
                    case "HardPoint/Loading/ShotsInBurst":
                        target.HardPoint.Loading.ShotsInBurst = int.Parse(weapondef.Value);
                        break;
                    case "HardPoint/Loading/DelayAfterBurst":
                        target.HardPoint.Loading.DelayAfterBurst = int.Parse(weapondef.Value);
                        break;
                    case "HardPoint/Loading/RateOfFire":
                        target.HardPoint.Loading.RateOfFire = int.Parse(weapondef.Value);
                        break;
                    case "HardPoint/Loading/BarrelSpinRate":
                        target.HardPoint.Loading.BarrelSpinRate = int.Parse(weapondef.Value);
                        break;
                    case "HardPoint/Loading/GiveUpAfter":
                        target.HardPoint.Loading.GiveUpAfter = bool.Parse(weapondef.Value);
                        break;
                    case "HardPoint/Loading/FireFull":
                        target.HardPoint.Loading.FireFull = bool.Parse(weapondef.Value);
                        break;
                    case "HardPoint/DelayCeaseFire":
                        target.HardPoint.DelayCeaseFire = int.Parse(weapondef.Value);
                        break;
                    case "HardPoint/DeviateShotAngle":
                        target.HardPoint.DeviateShotAngle = float.Parse(weapondef.Value);
                        break;
                    case "HardPoint/AimingTolerance":
                        target.HardPoint.AimingTolerance = double.Parse(weapondef.Value);
                        break;
                    case "HardPoint/Loading/HeatPerShot":
                        target.HardPoint.Loading.HeatPerShot = int.Parse(weapondef.Value);
                        break;
                    case "HardPoint/Loading/MaxHeat":
                        target.HardPoint.Loading.MaxHeat = int.Parse(weapondef.Value);
                        break;
                    case "HardPoint/Loading/HeatSinkRate":
                        target.HardPoint.Loading.HeatSinkRate = int.Parse(weapondef.Value);
                        break;
                    case "HardPoint/Loading/Cooldown":
                        target.HardPoint.Loading.Cooldown = float.Parse(weapondef.Value);
                        break;
                    case "HardPoint/Loading/StayCharged":
                        target.HardPoint.Loading.StayCharged = bool.Parse(weapondef.Value);
                        break;
                    case "HardPoint/Loading/MagsToLoad":
                        target.HardPoint.Loading.MagsToLoad = int.Parse(weapondef.Value);
                        break;                    
                    case "HardPoint/Ai/TrackTargets":
                        target.HardPoint.Ai.TrackTargets = bool.Parse(weapondef.Value);
                        break;
                    case "HardPoint/Ai/SuppressFire":
                        target.HardPoint.Ai.SuppressFire = bool.Parse(weapondef.Value);
                        break;
                    case "HardPoint/Ai/OverrideLeads":
                        target.HardPoint.Ai.OverrideLeads = bool.Parse(weapondef.Value);
                        break;
                    case "HardPoint/HardWare/RotateRate":
                        target.HardPoint.HardWare.RotateRate = float.Parse(weapondef.Value);
                        break;
                    case "HardPoint/HardWare/ElevateRate":
                        target.HardPoint.HardWare.ElevateRate = float.Parse(weapondef.Value);
                        break;
                    case "HardPoint/AimLeadingPrediction":
                        target.HardPoint.AimLeadingPrediction = (WeaponDefinition.HardPointDef.Prediction)Enum.Parse(typeof(WeaponDefinition.HardPointDef.Prediction), weapondef.Value);
                        break;
                    default:
                        FError($"fgs: ERROR default! [{weapondef.Variable}] Свойство не добавлено в импортер.");
                        break;
                }
            }
            catch (Exception ex)
            {
                SLog($"fgs: weapondef.PartName [{weapondef.Variable}] throw ex: {ex.Message}  u use float when expected int? ");
            }

        }

        private void SetAmmoDefMod(Modifiers.AmmoMod ammodef, ContainerDefinition baseDefs)
        {
            bool found = false;
            foreach (var item in baseDefs.WeaponDefs)
            {
                try
                {
                    foreach (var targetAmmo in item.Ammos)
                    {
                        if (targetAmmo == null)
                        {
                            FError($"fgs: ERROR  ammo == null");
                        }
                        if (targetAmmo.AmmoRound == null)
                        {
                            FError($"fgs: ERROR ammo.AmmoRound == null");
                        }



                        if (targetAmmo.AmmoRound == ammodef.AmmoName)
                        {
                            found = true;
                            ApplyAmmo(ammodef, targetAmmo);
                        }
                        else if (RemoveTier(targetAmmo.AmmoRound) == ammodef.AmmoName)
                        {
                            found = true;
                            ApplyAmmo(ammodef, targetAmmo);
                        }
                    }

                }
                catch (Exception ex)
                {
                    SLog($"fgs: ERROR Exception [{ex}]");
                }
            }

            if (!found)
                SLog($"fgs: ERROR NOT FOUND ammo: [{ammodef.AmmoName}]  + val: [{ammodef.Value}]");
        }

        private void SetWeaponDefMod(Modifiers.WeaponMod weapondef, ContainerDefinition baseDefs)
        {

            WeaponDefinition target = null;

            foreach (var item in baseDefs.WeaponDefs)
            {
                if (item.HardPoint.PartName == weapondef.PartName)
                {
                    foreach (var point in item.Assignments.MountPoints)
                    {
                        if (weapondef.BlockSubType.Contains(point.SubtypeId))
                        {
                            target = item;
                            break;
                        }
                        else if (weapondef.BlockSubType.Contains(RemoveTier(point.SubtypeId)))
                        {
                            target = item;
                            break;
                        }

                    }
                }
            }

            if (target == null)
            {
                FError(weapondef.PartName + " weapon not found in Weapon Core!");
                return;
            }

            ApplyWeapon(weapondef, target);

        }

        private static string RemoveTier(string s)
        {
            return s.Replace(" T01", "")
                //.Replace(" T02", "")
                //.Replace(" T03", "")
                //.Replace(" T04", "")
                .Replace("_T01", "");
            //.Replace("_T02", "")
            //.Replace("_T03", "")
            //.Replace("_T04", "");
        }

        private void PrintWepAmmoLink(ContainerDefinition baseDefs)
        {
            Dictionary<string, List<string>> keyValuePairs = new Dictionary<string, List<string>>();
            foreach (var a in baseDefs.WeaponDefs)
            {
                foreach (WeaponDefinition.AmmoDef b in a.Ammos)
                {
                    if (keyValuePairs.ContainsKey(b.AmmoRound))
                    {
                        if (!keyValuePairs[b.AmmoRound].Contains(a.HardPoint.PartName))
                        {
                            keyValuePairs[b.AmmoRound].Add(a.HardPoint.PartName);
                        }
                    }
                    else
                    {
                        keyValuePairs.Add(b.AmmoRound, new List<string>());
                        keyValuePairs[b.AmmoRound].Add(a.HardPoint.PartName);
                    }
                }
            }
            foreach (var AmmoMagazine in keyValuePairs)
            {

                var temp = "[" + AmmoMagazine.Key + "] ";
                foreach (var weap in AmmoMagazine.Value)
                {
                    temp += weap + ", ";
                }
                Log.CleanLine(temp);
            }
        }

        private static void ExportWeapons(ContainerDefinition baseDefs)
        {
            var kek = MyAPIGateway.Utilities.SerializeToXML(baseDefs.WeaponDefs);
            var file = MyAPIGateway.Utilities.WriteBinaryFileInWorldStorage("weapons.xml", typeof(string));
            file.Write(kek);
            file.Close();
        }

        public void ExportAmmo(ContainerDefinition baseDefs)
        {
            List<WeaponDefinition.AmmoDef> temp = new List<WeaponDefinition.AmmoDef>();

            foreach (var a in baseDefs.WeaponDefs)
            {
                foreach (WeaponDefinition.AmmoDef b in a.Ammos)
                {
                    if (!temp.Any(x => x.AmmoRound == b.AmmoRound))
                    {
                        temp.Add(b);
                    }
                }
            }

            var fileAmmo = MyAPIGateway.Utilities.WriteBinaryFileInWorldStorage("fileAmmo.xml", typeof(string));
            fileAmmo.Write(MyAPIGateway.Utilities.SerializeToXML(temp));
            fileAmmo.Close();
        }

        public class Modifiers
        {
            public AmmoMod[] Ammos;
            public WeaponMod[] Weapons;

            public struct AmmoMod
            {
                /// <summary>
                /// AmmoRound name
                /// </summary>
                public string AmmoName;
                public string Variable;
                public string Value;
            }

            public struct WeaponMod
            {
                public string BlockSubType;
                public string PartName;
                public string Variable;
                public string Value;
            }
        }

        public class Log
        {
            private static Log _instance = null;
            internal TextWriter File = null;

            public static void Init(string name)
            {
                _instance = new Log { File = MyAPIGateway.Utilities.WriteFileInLocalStorage(name, typeof(Log)) };
            }

            public static void CleanLine(string text)
            {
                _instance.File.WriteLine(text);
                _instance.File.Flush();
            }

            public static void Close()
            {
                if (_instance?.File == null) return;
                _instance.File.Flush();
                _instance.File.Close();
                _instance.File.Dispose();
                _instance.File = null;
                _instance = null;
            }
        }
    }
}

