﻿using System.Collections.Generic;
using VRageMath;

namespace Scripts
{
    public partial class Parts2
    {

        internal Structure.WeaponDefinition.AmmoDef.Randomize Random(float start, float end)
        {
            return new Structure.WeaponDefinition.AmmoDef.Randomize { Start = start, End = end };
        }

        internal Vector4 Color(float red, float green, float blue, float alpha)
        {
            return new Vector4(red, green, blue, alpha);
        }

        internal Vector3D Vector(double x, double y, double z)
        {
            return new Vector3D(x, y, z);
        }

        internal Structure.WeaponDefinition.AnimationDef.RelMove.XYZ Transformation(double X, double Y, double Z)
        {
            return new Structure.WeaponDefinition.AnimationDef.RelMove.XYZ { x = X, y = Y, z = Z };
        }

        internal Dictionary<Structure.WeaponDefinition.AnimationDef.PartAnimationSetDef.EventTriggers, uint> Delays(uint FiringDelay = 0, uint ReloadingDelay = 0, uint OverheatedDelay = 0, uint TrackingDelay = 0, uint LockedDelay = 0, uint OnDelay = 0, uint OffDelay = 0, uint BurstReloadDelay = 0, uint OutOfAmmoDelay = 0, uint PreFireDelay = 0, uint StopFiringDelay = 0, uint StopTrackingDelay = 0, uint InitDelay = 0)
        {
            return new Dictionary<Structure.WeaponDefinition.AnimationDef.PartAnimationSetDef.EventTriggers, uint>
            {
                [Structure.WeaponDefinition.AnimationDef.PartAnimationSetDef.EventTriggers.Firing] = FiringDelay,
                [Structure.WeaponDefinition.AnimationDef.PartAnimationSetDef.EventTriggers.Reloading] = ReloadingDelay,
                [Structure.WeaponDefinition.AnimationDef.PartAnimationSetDef.EventTriggers.Overheated] = OverheatedDelay,
                [Structure.WeaponDefinition.AnimationDef.PartAnimationSetDef.EventTriggers.Tracking] = TrackingDelay,
                [Structure.WeaponDefinition.AnimationDef.PartAnimationSetDef.EventTriggers.TurnOn] = OnDelay,
                [Structure.WeaponDefinition.AnimationDef.PartAnimationSetDef.EventTriggers.TurnOff] = OffDelay,
                [Structure.WeaponDefinition.AnimationDef.PartAnimationSetDef.EventTriggers.BurstReload] = BurstReloadDelay,
                [Structure.WeaponDefinition.AnimationDef.PartAnimationSetDef.EventTriggers.NoMagsToLoad] = OutOfAmmoDelay,
                [Structure.WeaponDefinition.AnimationDef.PartAnimationSetDef.EventTriggers.PreFire] = PreFireDelay,
                [Structure.WeaponDefinition.AnimationDef.PartAnimationSetDef.EventTriggers.EmptyOnGameLoad] = 0,
                [Structure.WeaponDefinition.AnimationDef.PartAnimationSetDef.EventTriggers.StopFiring] = StopFiringDelay,
                [Structure.WeaponDefinition.AnimationDef.PartAnimationSetDef.EventTriggers.StopTracking] = StopTrackingDelay,
                [Structure.WeaponDefinition.AnimationDef.PartAnimationSetDef.EventTriggers.LockDelay] = LockedDelay,
                [Structure.WeaponDefinition.AnimationDef.PartAnimationSetDef.EventTriggers.Init] = InitDelay,
            };
        }

        internal Structure.WeaponDefinition.AnimationDef.PartEmissive Emissive(string EmissiveName, bool CycleEmissiveParts, bool LeavePreviousOn, Vector4[] Colors, float IntensityFrom, float IntensityTo, string[] EmissivePartNames)
        {
            return new Structure.WeaponDefinition.AnimationDef.PartEmissive
            {
                EmissiveName = EmissiveName,
                Colors = Colors,
                CycleEmissivesParts = CycleEmissiveParts,
                LeavePreviousOn = LeavePreviousOn,
                EmissivePartNames = EmissivePartNames,
                IntensityRange = new[]{ IntensityFrom, IntensityTo }
            };
        }

        internal Structure.WeaponDefinition.AnimationDef.PartAnimationSetDef.EventTriggers[] Events(params Structure.WeaponDefinition.AnimationDef.PartAnimationSetDef.EventTriggers[] events)
        {
            return events;
        }

        internal string[] Names(params string[] names)
        {
            return names;
        }

        internal string[] AmmoRounds(params string[] names)
        {
            return names;
        }
    }
}