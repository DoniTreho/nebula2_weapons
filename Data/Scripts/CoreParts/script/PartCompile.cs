﻿using System.Collections.Generic;
using System.ComponentModel;
using VRageMath;
using static Scripts.Structure;
using static Scripts.Structure.WeaponDefinition.AmmoDef;
using static Scripts.Structure.WeaponDefinition.AnimationDef.RelMove;
using static Scripts.Structure.WeaponDefinition.AnimationDef.PartAnimationSetDef;
using static Scripts.Structure.WeaponDefinition.AnimationDef;
using static Scripts.Structure.WeaponDefinition.AnimationDef.PartAnimationSetDef.EventTriggers;
using static Scripts.Structure.ArmorDefinition.ArmorType;

namespace Scripts
{
    partial class Parts
    {
        internal ContainerDefinition Container = new ContainerDefinition();
        internal void PartDefinitions(params WeaponDefinition[] defs)
        {
            Container.WeaponDefs = defs;
        }

        internal void ArmorDefinitions(params ArmorDefinition[] defs)
        {
            Container.ArmorDefs = defs;
        }

        internal void SupportDefinitions(params SupportDefinition[] defs)
        {
            Container.SupportDefs = defs;
        }

        internal void UpgradeDefinitions(params UpgradeDefinition[] defs)
        {
            Container.UpgradeDefs = defs;
        }

        internal static void GetBaseDefinitions(out ContainerDefinition baseDefs)
        {
            baseDefs = new Parts().Container;
        }
        
        internal static void SetModPath(ContainerDefinition baseDefs, string modContext)
        {
            if (baseDefs.WeaponDefs != null)
                for (int i = 0; i < baseDefs.WeaponDefs.Length; i++)
                    baseDefs.WeaponDefs[i].ModPath = modContext;

            if (baseDefs.SupportDefs != null)
                for (int i = 0; i < baseDefs.SupportDefs.Length; i++)
                    baseDefs.SupportDefs[i].ModPath = modContext;

            if (baseDefs.UpgradeDefs != null)
                for (int i = 0; i < baseDefs.UpgradeDefs.Length; i++)
                    baseDefs.UpgradeDefs[i].ModPath = modContext;
        }

        
    }
}
